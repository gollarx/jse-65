package ru.t1.shipilov.tm.exception.user;

public final class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Error! Incorrect login/password entered. Please try again...");
    }

}
